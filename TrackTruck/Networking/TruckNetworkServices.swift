//
//  TrustNetworkServices.swift
//  TrackTruck
//
//  Created by Dheeraj on 23/10/21.
//

import UIKit

class TruckNetworkServices : ServiceManager{
    
    let apiURL = "https://api.mystral.in/tt/mobile/logistics/searchTrucks?auth-company=PCH&companyId=33&deactivated=false&key=g2qb5jvucg7j8skpu5q7ria0mu&q-expand=true&q-include=lastRunningState,lastWaypoint"
    
    func getTruckListService(_ searchText : String , completionHandler : @escaping (_ serviceReponse : ServiceResponse<[Truck], TruckError>) -> Void){

        self.requestWithUrlSession(apiURL, withParams: [:], andCompletion: { response in
            switch response{
                case .success(let data):
                    let decoder = JSONDecoder()
                    do {
                        /**Convert JSON into Data model*/
                        let res = try decoder.decode(TruckListResponse.self, from: data)
                        completionHandler(.success(res.data))
                    } catch {
                        print(error.localizedDescription)
                    }
                case .failure(let error):
                    completionHandler(.failure(.httpError(error)))
            }
        }
    )}
}

