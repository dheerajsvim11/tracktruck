import UIKit

class ServiceManager {
    
    private var dataTask : URLSessionDataTask?
    
    func requestWithUrlSession(_ strUrl: String, withParams params: [AnyHashable: Any], andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<Data, HTTPError>) -> Void){
        
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                completionBlock(.success(data!))
            }
            self?.dataTask = nil
        })
        dataTask!.resume()
    }
    
    func cancel(){
        if(dataTask != nil){
            dataTask?.cancel()
            dataTask = nil
        }
    }
}
