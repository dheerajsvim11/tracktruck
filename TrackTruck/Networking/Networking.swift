import Foundation

enum ServiceResponse<T,error : Error> {
    case success(T)
    case failure(error)
}

enum HTTPError : Error {
    case requestTimedOut
    case networkError
    case jsonError
}

enum TruckError : Error {
    case httpError(HTTPError)
    case dataNotFound
}
