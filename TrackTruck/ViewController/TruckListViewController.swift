//
//  ViewController.swift
//  TrackTruck
//
//  Created by Dheeraj on 23/10/21.
//

import UIKit
import GoogleMaps
import ProgressHUD


class TruckListViewController: UIViewController {
    
    lazy var truckListViewModel : TruckListViewModel = TruckListViewModel(delegate: self)

    @IBOutlet weak var tblTruckList: UITableView!
    var mapView: GMSMapView!

    lazy var searchBar:UISearchBar = UISearchBar()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**TableView Setup*/
        tableViewSetup()
        
        /***Setup search bar*/
        setupSearchBar()
        
        /**Setup segment control*/
        setupSegmentControl()
        
        /**Show HUD*/
        ProgressHUD.show("")
        /**Request truk list data from server*/
        truckListViewModel.requestTruckListData()
    }

    
    func tableViewSetup(){
        /**Hide initially*/
        tblTruckList.isHidden = true
        /***Truck List Setup*/
        tblTruckList.estimatedRowHeight = 120.0
        tblTruckList.rowHeight = UITableView.automaticDimension
        tblTruckList.tableFooterView = UIView()
        
    }
    func setupSegmentControl(){
        
        let titles = ["Map", "List"]
        let segmentControl = UISegmentedControl(items: titles)
        segmentControl.tintColor = UIColor.white
        segmentControl.selectedSegmentIndex = 0
        for index in 0...titles.count-1 {
            segmentControl.setWidth(120, forSegmentAt: index)
        }
        segmentControl.sizeToFit()
        segmentControl.addTarget(self, action: #selector(segmentChanged), for: .valueChanged)
        segmentControl.selectedSegmentIndex = 0
        navigationItem.titleView = segmentControl
        
        let menuButtonImage = UIImage(named: "reload")
            let menuButton = UIBarButtonItem(image: menuButtonImage, style: .plain, target: self, action: #selector(didTapReloadButton))
        navigationItem.rightBarButtonItem = menuButton
    }

    @objc func didTapReloadButton(sender: UIBarButtonItem) {
            
    }
    
    func setupSearchBar(){
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = "Truck Number"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        tblTruckList.tableHeaderView = searchBar
    }
    
    func setTruckLocations(){
        
        var bounds = GMSCoordinateBounds()
        let camera = GMSCameraPosition.camera(withLatitude: 11.11, longitude: 12.12, zoom: 14.0) //Set default lat and long
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: camera)
        self.view.addSubview(mapView)
        let arrTrucks = truckListViewModel.getTrucks()
        for truck in arrTrucks{
            let location = CLLocationCoordinate2D(latitude: truck.lastWaypoint.lat, longitude: truck.lastWaypoint.lng)
            print("location: \(location)")
            let marker = GMSMarker()
            marker.position = location
            marker.snippet = truck.truckNumber
            marker.map = mapView
            marker.icon = UIImage.init(named:truckListViewModel.getIconName(state: truck.lastRunningState.truckRunningState))
            bounds = bounds.includingCoordinate(marker.position)

        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        mapView.animate(with: update)
    }

    
    
    @objc func segmentChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            tblTruckList.isHidden = true
            mapView.isHidden = false
        }else{
            tblTruckList.isHidden = false
            mapView.isHidden = true
        }
    }
    
   
}


extension TruckListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return truckListViewModel.getTrucksCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TruckCell = tableView.dequeueReusableCell(withIdentifier: "TruckCell") as! TruckCell
        let truck = truckListViewModel.getTruck(index: indexPath.row)
        cell.configureCell(truck: truck,indexPath: indexPath)
        return cell
    }
}


extension TruckListViewController : UISearchBarDelegate{

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText:String){
        if !searchText.isEmpty{
            truckListViewModel.filterArray(searchString: searchText)
        }else{
            truckListViewModel.resetFilterData()
        }
        self.tblTruckList.reloadData();
    }
}


extension TruckListViewController : TruckListViewModelDelegate{
    
    func truckListResponseSuccess(){
        DispatchQueue.main.async {
            ProgressHUD.dismiss()
            self.tblTruckList.reloadData();
            self.setTruckLocations()
        }
    }
    
    func truckListResponseFailed(error: String){
        ProgressHUD.dismiss()
    }
}
