//
//  ResponseCode.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation

// MARK: - ResponseCode
struct ResponseCode: Codable {
    let responseCode: Int
    let message: String
}
