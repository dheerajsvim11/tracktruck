
import Foundation

// MARK: - Welcome
struct TruckListResponse: Codable {
    let responseCode: ResponseCode
    let data: [Truck]
}
