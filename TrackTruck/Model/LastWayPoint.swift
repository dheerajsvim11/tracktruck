//
//  LastWayPoint.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation

// MARK: - LastWaypoint
struct LastWaypoint: Codable {
    let lat, lng: Double
    let createTime: Int
    let speed: Double
    let ignitionOn: Bool
    
    enum CodingKeys: String, CodingKey {
        case lat, lng, createTime
        case speed, ignitionOn
    }
}
