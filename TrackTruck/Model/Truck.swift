//
//  Truck.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation

// MARK: - Datum
struct Truck: Codable {
    let id: Int
    let truckNumber: String
    let lastWaypoint: LastWaypoint
    let lastRunningState: LastRunningState


    enum CodingKeys: String, CodingKey {
        case id
        case truckNumber
        case lastWaypoint, lastRunningState
    }
}
