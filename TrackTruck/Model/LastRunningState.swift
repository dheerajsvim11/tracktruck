//
//  LastRunningState.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation

// MARK: - LastRunningState
struct LastRunningState: Codable {
    let stopStartTime, truckRunningState: Int

    enum CodingKeys: String, CodingKey {
        case stopStartTime, truckRunningState
    }
}
