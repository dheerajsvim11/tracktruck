//
//  Utilities.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation
import UIKit

class Utilities{
    static func getKMAttributedString(truck: Truck) -> NSMutableAttributedString{
        let amountValue = String(format: "%0.*f", 2, truck.lastWaypoint.speed)
        let attributedString = NSMutableAttributedString(string: "\(amountValue) k/h", attributes: nil)
        let myAttribute = [NSAttributedString.Key.foregroundColor: UIColor.red ]
        let strNS = "\(amountValue) k/h" as NSString
        attributedString.setAttributes(myAttribute, range: strNS.range(of: "k/h"))
        return attributedString
    }
}
