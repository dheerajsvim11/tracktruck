//
//  TruckViewModel.swift
//  TrackTruck
//
//  Created by Dheeraj on 26/10/21.
//

import Foundation


protocol TruckListViewModelDelegate : class{
    func truckListResponseSuccess()
    func truckListResponseFailed(error:String)

    //func loginfailWithError(message:String)
}

public final class TruckListViewModel{
    
    weak var delegate : TruckListViewModelDelegate?

    //MARK:- Public Functions
    init(delegate : TruckListViewModelDelegate?) {
        self.delegate = delegate
    }
    
    //MARK:- Public Variables

    var arrTrucks = [Truck]()
    var arrFilteredTrucks = [Truck]()
    
    /**PUBLIC FUNCTIONS*/
    public func requestTruckListData(){
        requestTruckList()
    }
    
    /**PRIVATE FUNCTIONS*/
    private func requestTruckList(){
        
        let truckNetworkServices = TruckNetworkServices()
        
        truckNetworkServices.getTruckListService("", completionHandler: { [weak self] (serviceResponse) in
            switch serviceResponse{
                case .success(let arrTrucks):
                   
                    self?.arrTrucks = arrTrucks
                    self?.arrFilteredTrucks = arrTrucks
                    self!.delegate?.truckListResponseSuccess()
            
                case .failure(let error):
                    let errMsg : String
                    switch error{
                        case .httpError(let httpErr):
                            switch httpErr{
                                case .jsonError:
                                errMsg = "Json Error"
                                
                                case .networkError:
                                errMsg = "Network Error"
                                
                                case .requestTimedOut:
                                errMsg = "Request Timed Out"
                            }
                        
                        case .dataNotFound:
                            errMsg = "Data not found"
                    }
                    self?.delegate?.truckListResponseFailed(error: errMsg)
            }
        })
    }
    
    public func filterArray(searchString:String){
        /**Filter the array for search string*/
        arrFilteredTrucks = arrTrucks.filter({ $0.truckNumber.contains(searchString)})
    }
    
    func getTrucks() ->[Truck]{
        return arrFilteredTrucks
    }
    
    func getTrucksCount() ->Int{
        return arrFilteredTrucks.count
    }
    
    func getTruck(index :Int) -> Truck{
        return arrFilteredTrucks[index]
    }
    
    func resetFilterData(){
        arrFilteredTrucks = arrTrucks;
    }
    
    func getIconName(state:Int) -> String{
        var strState = ""
        if state == 0{
            strState = "truck_blue.png"
        }else if state == 1{
            strState = "truck_green.png"
        }else if state == 2{
            strState = "truck_yellow.png"
        }else if state == 3{
            strState = "truck_red.png"
        }
        return strState
    }
}
