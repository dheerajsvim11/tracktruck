//
//  TruckCell.swift
//  TrackTruck
//
//  Created by Dheeraj on 24/10/21.
//

import UIKit

class TruckCell: UITableViewCell {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblSpeed: UILabel!
    @IBOutlet weak var lblTimeAgo: UILabel!
    @IBOutlet weak var lblStateLast: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        /**Set bottom space*/
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0))
    }
    
    func configureCell(truck: Truck, indexPath: IndexPath){
        
        /*Set truck number**/
        self.lblNumber.text = truck.truckNumber
        
        /**Set speed*/
        if truck.lastWaypoint.speed > 0{
            self.lblSpeed.isHidden = false
            /**Set attributed color for k/h in red color*/
            self.lblSpeed.attributedText = Utilities.getKMAttributedString(truck: truck)//"\(amountValue) k/h"

        }else{
            self.lblSpeed.isHidden = true
        }
        
    
        if truck.lastRunningState.stopStartTime > 0{
            var strState = "Stopped"
            if truck.lastRunningState.truckRunningState == 1{
                strState = "Running"
            }
            self.lblStateLast.text = strState + " since last " + CalenderUtility.setTimestamp(epochTime: String(truck.lastRunningState.stopStartTime))
        }
        
        if truck.lastWaypoint.createTime > 0 {
            self.lblTimeAgo.isHidden = false
            self.lblTimeAgo.text = CalenderUtility.setTimestamp(epochTime: String(truck.lastWaypoint.createTime))
        }else{
            self.lblTimeAgo.isHidden = true
        }
    }
}
